import { NgrxPPage } from './app.po';

describe('ngrx-p App', () => {
  let page: NgrxPPage;

  beforeEach(() => {
    page = new NgrxPPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
