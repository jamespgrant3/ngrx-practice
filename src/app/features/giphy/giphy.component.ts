import { Component, OnInit } from '@angular/core';
import { GiphyService } from 'app/core/services/giphy.service';
import * as fromRoot from 'app/core/reducers';
import { Observable } from 'rxjs/Observable';
import { Giphy } from 'app/core/models/giphy';
import { Router } from '@angular/router';

@Component({
  selector: 'app-giphy',
  templateUrl: './giphy.component.html',
  styleUrls: ['./giphy.component.scss']
})
export class GiphyComponent implements OnInit {
  searchTerm$: Observable<string>;
  giphys$: Observable<Giphy[]>;

  constructor(private giphyService: GiphyService,
              private router: Router){ }

  ngOnInit() {
    this.searchTerm$ = this.giphyService.getSearchTerm();
    this.giphys$ = this.giphyService.getGiphys();
  };

  giphySelected(giphy: Giphy): void {
    this.giphyService.setGiphySelected(giphy);
    this.router.navigate(['/giphy', giphy.id, 'view']);
  };

  searchTerm(term: string): void {
    this.giphyService.searchTerm(term);
  };
}
