import { Component, Input, OnInit } from '@angular/core';
import { Giphy } from 'app/core/models/giphy';

@Component({
  selector: 'app-giphy-detail',
  templateUrl: './giphy-detail.component.html',
  styleUrls: ['./giphy-detail.component.scss']
})
export class GiphyDetailComponent implements OnInit {
  @Input() giphy: Giphy;

  constructor() { }

  ngOnInit() {
    console.log('giphy', this.giphy);
  };
}
