import { Component, OnInit } from '@angular/core';
import { GiphyService }  from 'app/core/services/giphy.service';
import { Observable } from 'rxjs/Observable';
import { Giphy } from 'app/core/models/giphy';

@Component({
  selector: 'app-giphy-view',
  templateUrl: './giphy-view.component.html',
  styleUrls: ['./giphy-view.component.scss']
})
export class GiphyViewComponent implements OnInit {
  giphy$: Observable<Giphy>;

  constructor(private giphyService: GiphyService) { }

  ngOnInit() {
    this.giphy$ = this.giphyService.getGiphySelected();
  };
}
