import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { Giphy } from 'app/core/models/giphy';

@Component({
  selector: 'app-giphy-list',
  templateUrl: './giphy-list.component.html',
  styleUrls: ['./giphy-list.component.scss']
})
export class GiphyListComponent implements OnInit {
  @Input() giphys: Giphy[];
  @Output() giphySelected = new EventEmitter();

  constructor() { }

  ngOnInit() {

  };

  selectGiphy(giphy: Giphy){
    this.giphySelected.emit(giphy);
  };

}
