import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from 'app/core/reducers';
import * as GiphyActions from 'app/core/actions/giphy';

@Component({
  selector: 'app-giphy-search',
  templateUrl: './giphy-search.component.html',
  styleUrls: ['./giphy-search.component.scss']
})
export class GiphySearchComponent implements OnInit {
  @Input() term: string;
  @Output() termSearched = new EventEmitter();

  constructor(private store: Store<fromRoot.State>) { }

  ngOnInit() {
  };

  search(): void{
    this.termSearched.emit(this.term);
  };
}
