import { NgModule } from '@angular/core';

import { MdInputModule } from '@angular/material';
import { MdButtonModule } from '@angular/material';
import { MdCardModule } from '@angular/material';

@NgModule({
  imports: [
    MdInputModule,
    MdButtonModule,
    MdCardModule
  ],
  exports: [
    MdInputModule,
    MdButtonModule,
    MdCardModule
  ]
})
export class MaterialModule { }