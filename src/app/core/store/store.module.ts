import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducer } from 'app/core/reducers';
import { EffectsModule } from '@ngrx/effects';
import { GiphyEffects } from 'app/core/effects/giphy';

@NgModule({
  imports: [
    StoreModule.provideStore(reducer),
    EffectsModule.run(GiphyEffects),
    StoreDevtoolsModule.instrumentOnlyWithExtension()
  ],
  providers: [
    
  ]
})
export class AppStoreModule { }