import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { GiphyService } from 'app/core/services/giphy.service';
import { Observable } from 'rxjs/Observable';
import { Giphy } from 'app/core/models/giphy';

import 'rxjs/add/operator/catch';

@Injectable()
export class GiphyViewGuard implements CanActivate {
  constructor(private giphyService: GiphyService,
              private router: Router){ }

  canActivate(): Observable<boolean>{
    return this.check();
  };

  private check(): Observable<boolean> {
    return this.giphyService.getGiphySelected()
      .map((giphy: Giphy) => {
        const exists: boolean = !!giphy;

        // no selected giphy, route to search.
        if(!exists){
          this.router.navigate(['giphy']);
        }

        return exists;
      })
      .catch(() => {
        this.router.navigate(['giphy']);
        return Observable.of(false);
      });
  };
};