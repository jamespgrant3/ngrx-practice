export interface Giphy {
  id: string;
  type: string;
  embed_url: string;
};
