import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Giphy } from 'app/core/models/giphy';
import * as GiphyActions from 'app/core/actions/giphy';
import * as fromRoot from 'app/core/reducers';

@Injectable()
export class GiphyService {
  constructor(private store: Store<fromRoot.State>) { }

  getSearchTerm(): Observable<string> {
    return this.store.select(fromRoot.getSearchTerm);
  };

  getGiphys(): Observable<Giphy[]> {
    return this.store.select(fromRoot.getGiphys);
  };

  setGiphySelected(giphy: Giphy): void {
    this.store.dispatch(new GiphyActions.GiphySelectedAction(giphy));
  };

  searchTerm(term: string): void {
    this.store.dispatch(new GiphyActions.SearchGiphyAction(term));
  };

  getGiphySelected(): Observable<Giphy> {
    return this.store.select(fromRoot.getGiphySelected);
  };
};
