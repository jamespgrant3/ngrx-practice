import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Giphy } from 'app/core/models/giphy';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GiphyHttpService {
  constructor(private httpService: HttpService){ }

  search(searchTerm: string): Observable<Giphy[]>{
    return this.httpService.get('gifs/search', `q=${searchTerm}`)
      .map(response => <Giphy[]>response)    
  }
}