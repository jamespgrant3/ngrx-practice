import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Http, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class HttpService {
  constructor(private http: Http){ }
  
  get(url: string, parameters: string): Observable<any>{
    const params = new URLSearchParams(parameters);
    params.append('api_key', environment.apiKey);

    return this.http.get(`${environment.baseUrl}${url}?${params.toString()}`)
            .map(response => response.json().data);
  }
};
