import { Injectable } from '@angular/core';
import { GiphyHttpService } from 'app/core/services/http/giphy.service';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { Action } from '@ngrx/store';

import * as GiphyActions from 'app/core/actions/giphy';
import { Giphy } from 'app/core/models/giphy';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/observable/of';

@Injectable()
export class GiphyEffects {
  private baseUrl: string = 'http://api.giphy.com/v1/gifs/';
  private apiKey: string = 'dc6zaTOxFJmzC';

  constructor(private giphyHttpService: GiphyHttpService,
              private actions$: Actions){ }

  @Effect()
  searchGiphy$: Observable<Action> = this.actions$
    .ofType(GiphyActions.ActionTypes.SEARCH_GIPHY)
    .map((action: GiphyActions.SearchGiphyAction) => action.payload)
    .switchMap((searchTerm) => {
      return this.giphyHttpService.search(searchTerm)
        .map(giphys => new GiphyActions.SearchGiphySuccessfulAction(giphys));
        // .catch(() => Observable.of(new GiphyActions.SearchGiphyFailureAction({
        //   error: 'an error occurred fetching giphys'
        // })));
    });
}