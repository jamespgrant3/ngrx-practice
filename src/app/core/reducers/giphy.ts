import { Giphy } from 'app/core/models/giphy';
import * as GiphyActions from 'app/core/actions/giphy';

export interface State {
  giphys: Giphy[],
  selectedGiphy: Giphy,
  searchTerm: string
};

export const initialState: State = {
 giphys: [],
 selectedGiphy: null,
 searchTerm: null
};

export function reducer(state = initialState, action: GiphyActions.Actions): State {
  switch(action.type){
    case GiphyActions.ActionTypes.SEARCH_GIPHY: {
      const searchTerm: string = (<GiphyActions.SearchGiphyAction>action).payload;
      
      return {
        ...state,
        ...{ searchTerm: searchTerm }
      };
    };

    case GiphyActions.ActionTypes.SEARCH_GIPHY_SUCCESS: {
      const giphys = (<GiphyActions.SearchGiphySuccessfulAction>action).payload;
      return {
        ...state,
        ...{ giphys: giphys }
      };
    };

    case GiphyActions.ActionTypes.GIPHY_SELECTED: {
      const giphy = (<GiphyActions.GiphySelectedAction>action).payload;
      return {
        ...state,
        ...{ selectedGiphy: giphy }
      };
    };

    default: {
      return state;
    }
  };
};

export const getSearchTerm = (state: State) => state.searchTerm;
export const getGiphys = (state: State) => state.giphys;
export const getGiphySelected = (state: State) => state.selectedGiphy;