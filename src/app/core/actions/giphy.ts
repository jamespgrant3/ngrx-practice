import { Action } from '@ngrx/store';
import { Giphy } from 'app/core/models/giphy';

export const ActionTypes = {
  SEARCH_GIPHY: '[Giphy] Search',
  SEARCH_GIPHY_SUCCESS: '[Giphy] Search Successfully',
  SEARCH_GIPHY_FAILURE: '[Giphy] Search Failed',
  GIPHY_SELECTED: '[Giphy] Selected'
};

export class SearchGiphyAction implements Action {
  type = ActionTypes.SEARCH_GIPHY;

  constructor(public payload: string){ }
};

export class SearchGiphySuccessfulAction implements Action {
  type = ActionTypes.SEARCH_GIPHY_SUCCESS;

  constructor(public payload: Giphy[]){ }
};

export class SearchGiphyFailureAction implements Action {
  type = ActionTypes.SEARCH_GIPHY_FAILURE;
  constructor(public payload: { error: string }) { }
};

export class GiphySelectedAction implements Action {
  type = ActionTypes.GIPHY_SELECTED;
  constructor(public payload: Giphy){ }
};

export type Actions
  = SearchGiphyAction |
    SearchGiphySuccessfulAction |
    SearchGiphyFailureAction |
    GiphySelectedAction;
