import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppStoreModule } from 'app/core/store/store.module';
import { GiphyListComponent } from './features/giphy/giphy-list/giphy-list.component';

import { RouterModule }   from '@angular/router';
import { GiphyComponent } from 'app/features/giphy/giphy.component';
import { GiphyService } from 'app/core/services/giphy.service';
import { GiphySearchComponent } from './features/giphy/giphy-search/giphy-search.component';
import { GiphyDetailComponent } from './features/giphy/giphy-detail/giphy-detail.component';
import { GiphyViewComponent } from './features/giphy/giphy-view.component';
import { GiphyViewGuard } from 'app/core/guards/giphy-view';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'app/material.module';
import { HttpService } from 'app/core/services/http/http.service';
import { GiphyHttpService } from 'app/core/services/http/giphy.service';

@NgModule({
  declarations: [
    AppComponent,
    GiphyListComponent,
    GiphyComponent,
    GiphySearchComponent,
    GiphyDetailComponent,
    GiphyViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: '/giphy',
        pathMatch: 'full'
      },
      {
        path: 'giphy',
        component: GiphyComponent
      },
      {
        path: 'giphy/:id/view',
        canActivate: [GiphyViewGuard],
        component: GiphyViewComponent
      }
    ]),
    AppStoreModule,
    BrowserAnimationsModule,
    MaterialModule
  ],
  providers: [
    GiphyService,
    GiphyViewGuard,
    HttpService,
    GiphyHttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
